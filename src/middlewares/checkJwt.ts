import { Request, Response, NextFunction } from "express";
import * as jwt from "jsonwebtoken";
import config from "../config/config";
import { request } from "https";

export const checkJwt = (req: Request, res: Response, next: NextFunction) => {
    //Get JWT token from header
    const token = <string>req.headers["auth"];
    let jwtPayload;

    //Try to validate token and get data
    try {
        jwtPayload = <any>jwt.verify(token, config.jwtSecret);
        res.locals.jwtPayload = jwtPayload;
    } catch (error) {
        //If token is not valid, respond with 401 - Unauthorised
        res.status(401).send();
        return;
    }

    //Token is valid for 1 hour - A new token will be sent for every request
    const { userId, username } = jwtPayload;
    const newToken = jwt.sign({ userId, username }, config.jwtSecret, { expiresIn: "1h" });

    res.setHeader("token", newToken);

    //Call next middleware or COntroller
    next();
};