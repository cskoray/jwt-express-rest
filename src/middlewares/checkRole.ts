import { Request, Response, NextFunction } from "express";
import { getRepository } from "typeorm";
import { User } from "../entity/User";

export const checkRole = (roles: Array<string>) => {
    return async (req: Request, res: Response, next: NextFunction) => {
        //Get user Id from previous middleware
        const id = res.locals.jwtPayload.userId;

        //Get user role from DB
        const userRepository = getRepository(User);
        let user: User;

        try {
            user = await userRepository.findOneOrFail(id);
        } catch (id) {
            res.status(401).send();
        }

        //Check if user has one of the listed roles
        if (roles.indexOf(user.role) > -1) {
            next();
        } else {
            res.status(401).send();
        }
    };
};
