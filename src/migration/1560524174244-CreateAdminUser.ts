import { MigrationInterface, QueryRunner, getRepository } from "typeorm";
import { User } from "../entity/User";

export class CreateAdminUser1560524174244 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {

        let user = new User();
        user.username = "admin";
        user.password = "admin";
        user.role = "ADMIN";
        user.hashPassword();

        const userRepository = getRepository(User);
        await userRepository.save(user);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}

/*
    To run the migration script use below snippet

    $ npm run migration:run

*/