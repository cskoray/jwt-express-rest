import "reflect-metadata";
import { createConnection } from "typeorm";
import * as express from "express";
import * as bodyParser from "body-parser";
import * as helmet from "helmet";
import * as cors from "cors";
import routes from "./routes";

//Connects to DB then starts express
createConnection()
    .then(async connection => {
        //Creates a new express application instance
        const app = express();

        //Call middlewares
        app.use(helmet());
        app.use(cors());
        app.use(bodyParser.json());

        //Set all routes from routes folder
        app.use("/", routes);

        const port = 3000;
        app.listen(port, () => {
            console.log("Application started on " + port);
        })
    })
    .catch(error => console.log(error));