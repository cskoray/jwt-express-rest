import { Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import { getRepository } from "typeorm";
import { validate } from "class-validator";

import { User } from "../entity/User";
import config from "../config/config";

class AuthController {

    static login = async (req: Request, res: Response) => {
        //check if username & password is set
        let { username, password } = req.body;
        if (!(username && password)) {
            res.status(401).send()
        }

        //Get user from DB
        const userRepository = getRepository(User);
        let user: User;
        try {
            user = await userRepository.findOneOrFail({ where: { username } });
        } catch (error) {
            res.status(401).send();
        }

        //Check encrypted password
        if (!user.checkIfUnencryptedPasswordIsValid(password)) {
            res.status(401).send();
            return;
        }

        //Set JWT token for 1 hour
        const token = jwt.sign(
            { userId: user.id, username: user.username },
            config.jwtSecret,
            { expiresIn: "1h" }
        );

        //Send JWT token in response
        res.send(token);
    };

    static changePassword = async (req: Request, res: Response) => {

        //Get Id from JWT
        const id = res.locals.jwtPayload.userId;

        //Get parameters from body
        const { oldPassword, newPassword } = req.body;
        if (!(oldPassword && newPassword)) {
            res.status(400).send();
        }

        //Get user from DB
        const userRepository = getRepository(User);
        let user: User;
        try {
            user = await userRepository.findOneOrFail(id);
        } catch (id) {
            res.status(401).send();
        }

        //Check if old password matches
        if (!user.checkIfUnencryptedPasswordIsValid(oldPassword)) {
            res.status(401).send();
            return;
        }

        //Validate user (password length)
        user.password = newPassword;
        const errors = await validate(user);
        if (errors.length > 0) {
            res.status(400).send();
            return;
        }

        //Hash new password & save
        user.hashPassword();
        userRepository.save(user);
        res.status(204).send();
    };
};

export default AuthController;