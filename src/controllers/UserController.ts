import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { validate } from "class-validator";

import { User } from "../entity/User";
import { hash } from "bcryptjs";
import { urlencoded } from "body-parser";

class UserController {

    static listAll = async (req: Request, res: Response) => {

        //Get all users from DB
        const userRepository = getRepository(User);
        const users = await userRepository.find({
            select: ["id", "username", "role"]
        });

        //Respond users
        res.send(users);
    };

    static getOneById = async (req: Request, res: Response) => {

        //Get id from Url
        const id: number = req.params.id;

        //Get all users from DB
        const userRepository = getRepository(User);
        let user: User;
        try {
            user = await userRepository.findOneOrFail(
                id,
                {
                    select: ["id", "username", "role"]
                });
        } catch (error) {
            res.status(404).send("User not found");
        }
        //Respond user
        res.send(user);
    };

    static newUser = async (req: Request, res: Response) => {

        //Get parameters from body
        let { username, password, role } = req.body;
        let user = new User();
        user.username = username;
        user.password = password;
        user.role = role;

        //Validate fields
        const errors = await validate(user);
        if (errors.length > 0) {
            res.status(400).send();
            return;
        }

        //Hash user password
        user.hashPassword();
        const userRepository = getRepository(User);
        try {
            await userRepository.save(user);
        } catch (error) {
            res.status(400).send("Username already in use");
        }

        res.status(201).send("User created");
    };

    static editUser = async (req: Request, res: Response) => {

        //Get id from Url
        const id: number = req.params.id;

        //Get parameters from body
        let { username, role } = req.body;

        //Get user from DB
        const userRepository = getRepository(User);
        let user: User;
        try {
            user = await userRepository.findOneOrFail(id);
        } catch (error) {
            res.status(404).send("User not found");
        }

        //Validate new fields
        user.username = username;
        user.role = role;
        const errors = await validate(user);
        if (errors.length > 0) {
            res.status(400).send();
            return;
        }

        //Try to save
        try {
            await userRepository.save(user);
        } catch (error) {
            res.status(409).send("Username already in use");
            return;
        }

        res.status(204).send("User updated");
    };

    static deleteUser = async (req: Request, res: Response) => {

        //Get id from Url
        const id: number = req.params.id;

        //Get user from DB
        const userRepository = getRepository(User);
        let user: User;
        try {
            user = await userRepository.findOneOrFail(id);
        } catch (error) {
            res.status(404).send("User not found");
        }

        userRepository.delete(user);
        res.status(204).send("User deleted");
    };
};

export default UserController;